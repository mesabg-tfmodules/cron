terraform {
  required_version = ">= 0.13.2"
}

resource "aws_cloudwatch_event_rule" "event_rule_refresh_user_tokens" {
  name                = "${var.name.backend.slug}-refresh-user-tokens"
  description         = "Refresh ML token managed access"
  schedule_expression = "rate(5 hours)"
}

resource "aws_cloudwatch_event_target" "event_target_refresh_user_tokens" {
  target_id               = "${var.name.backend.slug}-refresh-user-tokens"
  rule                    = aws_cloudwatch_event_rule.event_rule_refresh_user_tokens.name
  arn                     = var.cluster.arn
  role_arn                = var.cluster.cloudwatch_execution_role

  ecs_target {
    task_count            = 1
    task_definition_arn   = aws_ecs_task_definition.ecs_task_definition_refresh_user_tokens.arn
    launch_type           = "FARGATE"

    network_configuration {
      subnets             = var.network.subnet.private
      security_groups     = [var.security.default]
    }
  }
}
